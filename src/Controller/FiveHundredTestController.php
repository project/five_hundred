<?php

namespace Drupal\five_hundred\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Test Controller.
 */
class FiveHundredTestController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $exceptionToThrow = new \Exception('Intentional 500!', 500);
    throw $exceptionToThrow;

  }

}

<?php

namespace Drupal\five_hundred\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for this module.
 */
class FiveHundredSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'five_hundred_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'five_hundred.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('five_hundred.settings');

    $form['five_hundred_html_file'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Path to 500 error page html file'),
      '#description' => $this->t("By default the contents of 500.html in this module's root directory will be shown when a fivehundred error is thrown.<br/>
        To override the location of the html file to use enter a path here.<br/>
        Enter path from drupal root and include a preceding forward slash.
        "),
      '#default_value' => $config->get('five_hundred_html_file'),
      '#suffix' => 'You can test the 500 error page <a href="/fivehundred-test" target="_blank">here</a>.',
    );

    $form['#validate'][] = '::validate_html_suffix';

    if ($form['five_hundred_html_file']['#default_value'] == '') {
      $form['five_hundred_html_file']['#default_value'] = '/' . drupal_get_path('module', 'five_hundred') . '/500.html';
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Verifies that the proposed file path ends in htm or html.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  function validate_html_suffix(&$form, $form_state) {
    if (!preg_match('/\.html?$/', $form['five_hundred_html_file']['#value']) && $form['five_hundred_html_file']['#value'] != '') {
      $form_state->setErrorByName('five_hundred_html_file', $this->t('Must be a .htm or .html file.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('five_hundred.settings');
    $config->set('five_hundred_html_file', $form_state->getValue('five_hundred_html_file'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

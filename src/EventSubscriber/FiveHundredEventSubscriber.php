<?php

namespace Drupal\five_hundred\EventSubscriber;

use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Handles exeption events.
 */
class FiveHundredEventSubscriber extends HttpExceptionSubscriberBase {

  /**
   * FiveHundredEventSubscriber constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to retrieve the current request.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct($request_stack, $config_factory) {

    if (
      NULL !== $request_stack->getCurrentRequest()->attributes->get('exception')->getCode()
      && (
        $request_stack->getCurrentRequest()->attributes->get('exception')->getCode() == 500
        || $request_stack->getCurrentRequest()->attributes->get('exception')->getCode() == 0
      )
    ) {
      $response = new Response();

      // Get contents of html file the user has selected.
      $userSetHtmlPath = $config_factory->get('five_hundred.settings')->get('five_hundred_html_file');
      // Use the path the user set if available.
      if ($userSetHtmlPath != '' && file_exists(DRUPAL_ROOT . $userSetHtmlPath)) {
        $errorDocumentHtml = file_get_contents(DRUPAL_ROOT . $userSetHtmlPath);
      }
      // Default html file.
      else {
        $errorDocumentHtml = file_get_contents(DRUPAL_ROOT . '/' . drupal_get_path('module', 'five_hundred') . '/500.html');
      }

      $response->setContent($errorDocumentHtml);
      $response->setStatusCode(500, '500 Internal Server Error');
      $response->send();
      die();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats() {
    return array();
  }

}

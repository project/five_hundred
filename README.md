
INTRODUCTION
------------

The Five Hundred Module offers customizable error pages for 500 errors.
To maximize success rate a static page is served.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
Visit the module setting page (/admin/config/five_hundred).
Set the path to an html file to serve as your error page.
A default html file is provided.

CONFIGURATION
-------------

There is one setting for this module: the path to an html file you want to use.
By default this points to 500.html in this module's root directory.
The configuration page has a link to generate an error for testing.

MAINTAINERS
-----------

Current maintainers:
 * Dorian Damon (damondt) - https://www.drupal.org/u/damondt
